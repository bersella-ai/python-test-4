# Python Test 4

本 Python 脚本用于对“毛玉线圈物语”服务器[2021 年劳动节活动](https://bbs.craft.moe/d/2323-2021-adventurer-legends-210418)过程中产生的战斗记录进行分析，并给出关键数据。

附件包含：

- `WHATT4.py` 脚本所在地。运行之，即可生成.CSV表格。
- `match1.txt` 正式比赛#1期间，游戏产生的聊天栏记录。
- `match2.txt` 正式比赛#2期间的记录。
- `match3.txt` 娱乐赛#3的记录。
- `match4.txt` 娱乐赛#4的记录。
- [CSV表格预览](https://docs.qq.com/sheet/DV0ZjbVlUZmpkdXBU)

### 注意：

- 脚本生成的结果仅供参考，不代表管理组立场。
- **脚本默认仅生成第二场比赛的结果（match2.csv）。** 如需统计其它比赛战报，应于脚本内修改`input_file`与`output_file`为相应战报和统计结果。
-  **此脚本对连击得分，和（事实上的）连击广播次数做了修正**：有时，当一人同时击败多人时，管理得分的命令方块可能会多计分，体现为瞬间出现的多条连击广播之“连击数”相同。修正后，排名高的队伍得分其实略低于[管理组统计](https://bbs.craft.moe/d/2342)（奖励按1:10换算）；排名低者则基本吻合。
- 制造12连击得分不明，在此假定为 5 分计算。
- 因分组情况丢失，暂未加入以队伍为单位的统计数据。
  + *在预览页面对第一、二场人工添加了队伍编号，但仍不予统计。*
- 出于技术原因，对职业的判断仅限于以主武器得分的记录。
- 同样出于技术原因，以 `0` 开头的玩家 ID ，在生成的 CSV 表格内将会被“精简”开头的 `0` （例如 `02022`）。不便之处，敬请谅解。  
在上述预览页面，该 ID 已被人工更正。